// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_svg/flutter_svg.dart';

void main() {
  runApp(_MyApp());
}

class _MyApp extends StatelessWidget {
  final String assetName = 'assets/images/flutter_logo.svg';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        body: Center(
          child: CarouselSlider(
            options: CarouselOptions(height: 400.0),
            items: [
              SvgPicture.network(
                  'https://raw.githubusercontent.com/dnfield/flutter_svg/7d374d7107561cbd906d7c0ca26fef02cc01e7c8/example/assets/flutter_logo.svg?sanitize=true'),
              SvgPicture.asset(assetName),
            ]
          )
        ),
      ),
    );
  }
}
